" Install Plugins
source $HOME/.vim/bundles.vim

" Colors
source $HOME/.vim/colors.vim
" Delete extra whitespaces source $HOME/.vim/trailing_whitespaces.vim
" Terminal helper function
source $HOME/.vim/terminal_config.vim

" Configure ctags
set tags=./.tags,.tags,./tags,tags

" 2 spaces for indenting
set shiftwidth=2

" 2 stops
set tabstop=2

" 4 spaces for elm
autocmd FileType elm setlocal shiftwidth=4 tabstop=4


" Disable mouse click to go to position
set mouse-=a

" Spaces instead of tabs
set expandtab

" Ignore files
set wildignore+=*/bower_vendor_libs/**
set wildignore+=*/vendor/**
set wildignore+=*/node_modules/**
set wildignore+=*/elm-stuff/**

" Spell check
set spell spelllang=en_us

" Vim command line size
set noshowmode

" Numbers
set number relativenumber

" Plugins config

" Lightline
source $HOME/.vim/lightline.vim

" Sintax range
:com -range JS <line1>,<line2>SyntaxInclude javascript
:com -range CSS <line1>,<line2>SyntaxInclude css

" Gundo map
nnoremap ,u :GundoToggle<CR>

" Deoplete
" call deoplete#custom#source('ultisnips', 'matchers', ['matcher_fuzzy'])
" call deoplete#custom#option('omni_patterns', {
"   \ 'elm': '[^ \t]+',
" \})
" call deoplete#custom#option('sources', {
"   \ 'elm': ['omni', 'tag', 'buffer', 'ultisnips'],
" \})
" call deoplete#custom#source('omni', 'functions', {
"   \ 'elm':  'elm#Complete',
" \})
" call deoplete#custom#source('tag', 'max_abbr_width', 0)
" call deoplete#custom#source('tag', 'max_menu_width', 0)
" call deoplete#custom#source('tag', 'max_list', 20)
" let deoplete#tag#cache_limit_size = 5000000
" let g:deoplete#enable_at_startup = 1
"
" Typescript syntax
autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear

" Elm vim
let g:elm_detailed_complete = 1
let g:elm_format_autosave = 1

" Ale custom icons
let g:ale_sign_error = ''
let g:ale_sign_warning = ''
let g:ale_echo_msg_error_str = ''
let g:ale_echo_msg_warning_str = ''
let g:ale_echo_msg_format = '  %severity%    %s'
let g:ale_lint_on_text_changed = 'never'
let g:ale_completion_enabled = 0

" Indent guides
let g:indent_guides_enable_on_vim_startup = 1

" Ale config
let g:ale_completion_enabled = 1
autocmd FileType elm let b:ale_linters = {'elm': ['make']}
autocmd FileType javascript let g:ale_fixers = {
\   'javascript': [ 'eslint'
\                 , 'prettier'
\                 , 'remove_trailing_lines'
\                 , 'trim_whitespace'],
\}

autocmd FileType typescript let b:ale_linters = {'typescript': ['tsserver', 'eslint']}
autocmd FileType typescript let g:ale_fixers = {
\   'typescript': [ 'eslint'
\                 , 'prettier'
\                 , 'remove_trailing_lines'
\                 , 'trim_whitespace'],
\}

let g:ale_fix_on_save = 1

" UtilSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetsDir="~/.vim/snips"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "snips"]

" Markdonw
autocmd FileType markdown let g:indentLine_enabled=0
autocmd FileType markdown setlocal conceallevel=0
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_fenced_languages = ['elm=elm', 'js=javascript']

" FZF config
let g:fzf_layout = { 'window': '-tabnew' }
nmap <silent> ,f :GFiles<CR>

" Multi-Surround
let g:surround_{char2nr('m')} = "\1Surround: \1\r\1\1"
let g:surround_{char2nr('M')} = "\1S-Open: \1\r\2S-Close: \2"

" Diable json conceals
let g:vim_json_syntax_conceal = 0

" Tagbar
nnoremap ,t :TagbarToggle<CR>

" Gutentags
let g:gutentags_project_root = ['elm-package.json', 'elm.json']
let g:gutentags_add_default_project_roots = 1

" COC
autocmd FileType typescript let b:coc_root_patterns = ['.eslintrc']
autocmd FileType typescript.tsx let b:coc_root_patterns = ['.eslintrc']
let g:coc_global_extensions = [
  \ 'coc-tsserver',
  \ 'coc-prettier',
  \ 'coc-eslint',
  \ 'coc-json',
  \ 'coc-solargraph',
  \ ]
nmap <silent> gd <Plug>(coc-definition)
nnoremap <silent> ,s :<C-u>CocList -I symbols<cr>
nmap <silent> ,k <Plug>(coc-diagnostic-prev)
nmap <silent> ,j <Plug>(coc-diagnostic-next)
nmap <silent> ,d <Plug>(coc-codeaction)<CR>
