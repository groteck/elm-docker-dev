export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="pygmalion"
CASE_SENSITIVE="true"
COMPLETION_WAITING_DOTS="true"
plugins=(git nvm node elm yarn tmux)

source $ZSH/oh-my-zsh.sh
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:$HOME/.yarn/bin"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

export EDITOR='vim'
export FZF_DEFAULT_COMMAND='ag -g ""'
export ZSH_TMUX_AUTOSTART=true

alias tmux="TERM=screen-256color-bce tmux"

alias vzsh="vim ~/.zshrc"
alias vv="vim ~/.vimrc"

# start tmux if already exist
if [[ -z "$TMUX" ]]
then
  ID="`tmux ls | grep -vm1 attached | cut -d: -f1`"
  if [[ -z "$ID" ]]
  then
    TERM=screen-256color-bce tmux new-session
  else
    TERM=screen-256color-bce tmux attach-session -t "$ID"
  fi
fi

# Git alias
alias git-clean='git branch --merged master | ' \
                'grep -v "\* master" | ' \
                'xargs -n 1 git branch -d'

eval "$(direnv hook zsh)"
source "/etc/profile.d/rvm.sh"
