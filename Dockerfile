FROM ubuntu:18.04

## Use bash instead sh
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

#############################
## Install ubuntu packages ##
#############################

RUN apt-get update && apt-get install -y \
  autoconf \
  automake \
  binutils \
  bison \
  build-essential \
  curl \
  direnv \
  gcc \
  gcc \
  git \
  libjansson-dev \
  libpq-dev \
  libxml2 \
  libxml2-dev \
  locales \
  make \
  make \
  mercurial \
  musl-dev \
  openssh-server \
  silversearcher-ag \
  software-properties-common \
  sudo \
  tmux \
  tree \
  libyaml-dev \
  zsh \
&& \
  add-apt-repository -y ppa:neovim-ppa/unstable && \
  apt-get update && \
  apt-get install -y neovim python-dev python-pip python3-pip python3-dev && \
  pip2 install neovim && \
  pip3 install neovim


######################
## Create sudo user ##
######################

# SSH user
ENV USERNAME groteck
ENV USERPASSWORD  groteck
ENV USERID 1000
# Create and configure user
RUN useradd -ms $(which zsh) $USERNAME
# Change user id
RUN usermod -u $USERID $USERNAME
RUN groupmod -g $USERID $USERNAME
# User with empty password
RUN echo "$USERNAME:$USERPASSWORD" | chpasswd
# Enable passwordless sudo for user
RUN \
  mkdir -p /etc/sudoers.d && \
  echo "$USERNAME ALL= NOPASSWD: ALL" > /etc/sudoers.d/$USERNAME && \
  chmod 0440 /etc/sudoers.d/$USERNAME

##########
## SSH ##
##########

# config sshd
RUN mkdir /var/run/sshd
EXPOSE 22

###########
## Ports ##
###########

EXPOSE 3000
EXPOSE 35729

##############################
## Sudo user configurations ##
##############################

USER $USERNAME
WORKDIR /home/$USERNAME

#########################
## Add ssh known hosts ##
#########################

# Create the needed directory and assign permissions
RUN mkdir /home/$USERNAME/.ssh
RUN chmod 700 /home/$USERNAME/.ssh
# Add github and bitbucket hosts
RUN ssh-keyscan -H github.com >> /home/$USERNAME/.ssh/known_hosts

#############
## Locales ##
#############

RUN sudo locale-gen en_US.UTF-8
RUN sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

#########
## ELM ##
#########
ENV NVMV 0.33.11
ENV NODE_CMD source ~/.nvm/nvm.sh &&
ENV NODE_VERSION 10.13.0

## Install NVM
RUN curl https://raw.githubusercontent.com/creationix/nvm/v$NVMV/install.sh | bash

## Install node
RUN /bin/bash -l -c "$NODE_CMD nvm install $NODE_VERSION"
RUN /bin/bash -l -c "$NODE_CMD nvm alias default $NODE_VERSION"
RUN curl -o- -L https://yarnpkg.com/install.sh | bash || true

###############
## PhraseApp ##
###############

ENV PHRASE_VERSION 1.12.2
ENV PHRASE_CLI_URL https://github.com/phrase/phraseapp-client/releases/download/$PHRASE_VERSION/phraseapp_linux_amd64.tar.gz

RUN \
  curl -L -o phraseapp.tar.gz "$PHRASE_CLI_URL" && \
  tar zxvf phraseapp.tar.gz && \
  rm phraseapp.tar.gz && \
  sudo mv phraseapp_linux_amd64 /usr/bin/phraseapp

###########
## CTAGS ##
###########
RUN \
  git clone http://github.com/universal-ctags/ctags.git ~/ctags && \
  cd ~/ctags && \
  ./autogen.sh && \
  ./configure --program-prefix=u && \
  sudo make && sudo make install && \
  # cleanup
  cd ~ && sudo rm -rf ctags


######################
## Personal config  ##
######################

ENV GITHUB_USER https://raw.githubusercontent.com/groteck

RUN wget -O - $GITHUB_USER/tmux-conf/master/install.sh | zsh

COPY gitignore .gitignore
RUN git config --global core.excludesfile ~/.gitignore && \
  git config --global user.email "groteck@gmail.com" && \
  git config --global user.name "Juan Fraire" && \
  git config --global push.default current

RUN sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
RUN sudo update-alternatives --config vi
RUN sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
RUN sudo update-alternatives --config vim
RUN sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
RUN sudo update-alternatives --config editor

RUN mkdir .vim .vim/bundle .vim/backup .vim/swap .vim/cache .vim/undo
RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
COPY vim .vim
COPY vimrc.vim .vimrc
RUN mkdir -p ~/.config
RUN ln -s ~/.vim ~/.config/nvim
RUN ln -s ~/.vimrc ~/.config/nvim/init.vim
RUN vim -N -u ~/.vim/bundles.vim +PlugInstall +qall

RUN curl -fLo ~/.local/share/nvim/site/spell/en.utf-8.spl --create-dirs \
  http://ftp.vim.org/pub/vim/runtime/spell/en.utf-8.spl
RUN curl -fLo ~/.local/share/nvim/site/spell/en.utf-8.sug --create-dirs \
  http://ftp.vim.org/pub/vim/runtime/spell/en.utf-8.sug

RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
RUN rm ~/.zshrc
COPY zshrc .zshrc

COPY exportD /usr/bin/exportD
RUN sudo chmod +x /usr/bin/exportD

USER root

CMD /bin/bash -l -c "/usr/sbin/sshd -D -e"
